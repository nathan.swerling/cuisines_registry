# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).

## Scaling

To scale up I would tend to use a Lucene based Solution, possibly Lucene itself or possibly neo4j.
Reasons:
+ The interesting part of the application is the connections between the objects, rather than the pieces of data themselves. This makes it less suitable for a relational DB based solution and better suited to something graph based. Querying a relational db with large amounts of data over multiple tables will not perform as well, and will not scale as well as a graph based solution.
+ I have had good experience with Lucene scaling searches up to around 80 Million pieces of data, with diverse connections. The system remained stable under increasing load, and was easy to parallelise
+ Lucene offers much of the boiler plate code for scaling and running in parallel, so we would not have to start from scratch.

I would look into breaking down the shear magnatude by using country based servers and propagating to the rest of the world as needed in near time, as opposed to trying to create one single and absolute instance. People in Berlin will be more likely to book a restaurant in Berlin, and not in Mexico City, so the focus should be on making the local bookings fast, even if the system allows you to book anywhere in the world.