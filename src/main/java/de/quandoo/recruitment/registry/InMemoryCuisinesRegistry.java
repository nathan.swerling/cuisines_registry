package de.quandoo.recruitment.registry;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	private List<List<Cuisine>> ranking = new ArrayList<List<Cuisine>>();
	 
	 public InMemoryCuisinesRegistry() {
		 List<Cuisine> newPeers = new ArrayList<Cuisine>();
		 ranking.add(newPeers);
	 }
	 
	@Override
	public synchronized void register(Customer customer, Cuisine cuisine) {

		/*
		 * We only want to increment the popularity count if the customer was
		 * not previously registered for this cuisine.
		 * 
		 * Using a Boolean to avoid calling method logic in an "if" statement
		 * 
		 */
		
		List<Cuisine> newPeers = null;
		Integer popularity = cuisine.getCustomers().size();
		Boolean customerAdded = cuisine.addCustomer(customer);
		if (customerAdded) {
			customer.addCuisine(cuisine);
			// cuisine.customers is initialised to a non null object
			// and has no setter, so it should always be non null.
				List<Cuisine> oldPeers = ranking.get(popularity);
				oldPeers.remove(cuisine);
			if (ranking.size() == popularity + 1) {
				newPeers = new ArrayList<Cuisine>();
				ranking.add(newPeers);
			} else {
				newPeers = ranking.get(popularity + 1);
			}
			newPeers.add(cuisine);
		}
	}

	@Override
	public List<Customer> cuisineCustomers(Cuisine cuisine) {
		List<Customer> customers = null;
		if (cuisine != null) {
			customers = cuisine.getCustomers();
		}
		return customers;
	}

	@Override
	public List<Cuisine> customerCuisines(Customer customer) {
		List<Cuisine> cuisines = null;
		if (customer != null) {
			cuisines = customer.getCuisines();
		}
		return cuisines;
	}

	@Override
	public List<Cuisine> topCuisines(int n) {
		List<Cuisine> top = new ArrayList<Cuisine>();
		int sizeOfRanking = ranking.size();
		int i = 0;
		int j = 1;
		while ( i < n) {
			List<Cuisine> list = ranking.get(sizeOfRanking - j);
			if (list.size() > 0) {
				for (Cuisine cuisine : list) {
					top.add(cuisine);
				}
				i++;
			}
			j++;
		}
		return top;
	}
}
