package de.quandoo.recruitment.registry.model;

import java.util.ArrayList;
import java.util.List;

public class Cuisine {
	
    private String name;

	/* If we are to have an indefinite number of cuisines, then we need each cuisine to 
       reference their customers. This List can be added to or customers can be deleted 
       out of it, but the list itself should remain the same instance of ArrayList throughout
       a cuisine's lifetime, so it is initialised with a new ArrayList on creation. This could
       could be done in a constructor, but that makes for more unnecessary code, and means an
       additional method call at creation time where it is not strictly necessary. This way we
       do not need to overwrite the default constructor.
    */  
    private List<Customer> customers = new ArrayList<Customer>();
    
    private Cuisine() {
    	//do nothing. this prevents cuisines without names.
    }
    
    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    /*
     * only add customer if the customer is not already in the customer list.
     * This may need to be adapted depending on how the system is used, and whether or not
     * object (pointer) identity is sufficient, or whether we check customer identity by name
     * and address, or email address, or some other means. In this case it might be worthwhile
     * overwriting Customer.equals(Object) so as to keep the rest of the code simpler
     * 
     */
    
    public Boolean addCustomer(Customer customer) {
    	
    	Boolean success = false;
    	if (!customers.contains(customer)) {
    		customers.add(customer);
    		success = true;
    	}
    	return success;
    	
    }

    public List<Customer> getCustomers() {
    	return customers;
    }
}
