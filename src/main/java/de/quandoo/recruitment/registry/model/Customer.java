package de.quandoo.recruitment.registry.model;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	
	private String eMail = null;

	private List<Cuisine> cuisines = new ArrayList<Cuisine>();
	
	private Customer () {
		/* Do nothing. this is just to be sure that no customers
		 * are EVER created without an eMail address. 
		 */
	}

	public Customer(String eMail) {
		this.eMail = eMail;
	}
	
	public String getEMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public List<Cuisine> getCuisines() {
		return cuisines;
	}
	
	public Boolean addCuisine(Cuisine cuisine) {
		Boolean success = false;
		if (!cuisines.contains(cuisine)) {
			cuisines.add(cuisine);
			success = true;
		}
		return success;
	}
}
