package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import junit.framework.Assert;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.junit.Before;

public class InMemoryCuisinesRegistryTest {
	private final static Logger LOGGER = Logger.getLogger(InMemoryCuisinesRegistryTest.class.getName());

	private List<Customer> customers = new ArrayList<Customer>();

	private List<Cuisine> cuisines = new ArrayList<Cuisine>();

	private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

	@Before
	public void setup() {

		Cuisine french = new Cuisine("french");
		Cuisine german = new Cuisine("german");
		Cuisine italian = new Cuisine("italian");
		Cuisine thai = new Cuisine("thai");
		Cuisine japanese = new Cuisine("japanese");
		Cuisine burger = new Cuisine("burger");
		Cuisine fish = new Cuisine("fish");
		Cuisine vegan = new Cuisine("vegan");
		Cuisine fastFood = new Cuisine("fastFood");

		Customer c1 = new Customer("1");
		Customer c2 = new Customer("2");
		Customer c3 = new Customer("3");
		Customer c4 = new Customer("4");
		Customer c5 = new Customer("5");
		Customer c6 = new Customer("6");
		Customer c7 = new Customer("7");
		Customer c8 = new Customer("8");
		Customer c9 = new Customer("9");
		Customer c10 = new Customer("10");

		customers.add(c1);
		customers.add(c2);
		customers.add(c3);
		customers.add(c4);
		customers.add(c5);

		cuisines.add(burger);
		cuisines.add(fish);
		cuisines.add(vegan);
		cuisines.add(fastFood);
		cuisines.add(french);
		cuisines.add(german);
		cuisines.add(italian);
		cuisines.add(thai);
		cuisines.add(japanese);

		cuisinesRegistry.register(c1, french);
		cuisinesRegistry.register(c1, french);
		cuisinesRegistry.register(c2, french);
		cuisinesRegistry.register(c2, german);

		cuisinesRegistry.register(c1, italian);
		cuisinesRegistry.register(c1, thai);
		cuisinesRegistry.register(c4, german);
		cuisinesRegistry.register(c5, thai);
		cuisinesRegistry.register(c5, french);

		cuisinesRegistry.register(c3, japanese);
		cuisinesRegistry.register(c4, japanese);
		cuisinesRegistry.register(c5, japanese);
		cuisinesRegistry.register(c6, japanese);
		cuisinesRegistry.register(c3, french);
		cuisinesRegistry.register(c5, french);

		cuisinesRegistry.register(c1, thai);
		cuisinesRegistry.register(c2, thai);
		cuisinesRegistry.register(c3, thai);
		cuisinesRegistry.register(c4, thai);
		cuisinesRegistry.register(c5, thai);
		cuisinesRegistry.register(c6, thai);
		cuisinesRegistry.register(c7, thai);
		cuisinesRegistry.register(c8, thai);
		cuisinesRegistry.register(c9, thai);
		cuisinesRegistry.register(c10, thai);
		cuisinesRegistry.register(c1, vegan);
		cuisinesRegistry.register(c1, burger);
		cuisinesRegistry.register(c1, fastFood);

		cuisinesRegistry.register(c1, fish);
		cuisinesRegistry.register(c2, fish);
		cuisinesRegistry.register(c3, fish);
		cuisinesRegistry.register(c4, fish);
		cuisinesRegistry.register(c5, fish);
		cuisinesRegistry.register(c6, fish);
		cuisinesRegistry.register(c7, fish);
		cuisinesRegistry.register(c8, fish);
		cuisinesRegistry.register(c9, fish);
		cuisinesRegistry.register(c10, fish);

	}

	@Test
	public void testNullCuisine() {
		cuisinesRegistry.cuisineCustomers(null);
	}

	@Test
	public void testNullCustomer() {
		cuisinesRegistry.customerCuisines(null);
	}

	@Test
	public void testTopCuisines1() {
		List<Cuisine> top1 = cuisinesRegistry.topCuisines(1);
		for (Cuisine cuisine : top1) {
			assertTrue(cuisine.getName().equals("thai") || cuisine.getName().equals("fish"));
		}
	}

	@Test
	public void testTopCuisines2() {
		List<Cuisine> top2 = cuisinesRegistry.topCuisines(2);
		assertTrue(top2.size() == 4);
	}

	@Test
	public void testTopCuisines3() {
		List<Cuisine> top3 = cuisinesRegistry.topCuisines(3);
		for (Cuisine cuisine : top3) {
			LOGGER.info(cuisine.getName() + " " + cuisine.getCustomers().size());
		}
	}

	@Test
	public void testTopCuisines4() {
		List<Cuisine> top4 = cuisinesRegistry.topCuisines(4);
		for (Cuisine cuisine : top4) {
			LOGGER.info(cuisine.getName() + " " + cuisine.getCustomers().size());
		}
	}

	@Test
	public void testCustomerCuisines1() {
		Customer customer = customers.get(0);
		List<Cuisine> cusCuisines1 = cuisinesRegistry.customerCuisines(customer);
		assertTrue(cusCuisines1.size() == 7);
	}

}